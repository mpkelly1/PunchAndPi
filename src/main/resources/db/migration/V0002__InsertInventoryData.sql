insert into clothing_inventory (title, size, color, price, inventory_remaining, product_img) values 
('Arctic Bandito', 'Adult', 'Larkspur', 20.00, 12, 'src/main/resources/db/migration/blue.jpg'),
('Arctic Bandita', 'Adult', 'Carnation', 20.00, 8, 'src/main/resources/db/migration/pink.jpg');