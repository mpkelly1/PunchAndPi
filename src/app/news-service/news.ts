export class News {
    newsId: number;
    title: string;
    body: string;
    createdOn: number;
    lastUpdated: string;
    newsImg: number;
}
