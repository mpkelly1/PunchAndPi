export class Post {
    postId: number;
    title: string;
    body: string;
    createdOn: number;
    lastUpdated: string;
    postImg: number;
}
